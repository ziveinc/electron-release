#!/usr/bin/env node

const version = require('./package').version

const fs = require('fs')
const os = require('os')
const path = require('path')
const extract = require('extract-zip')
const { download } = require('@electron/get')

if (process.env.ELECTRON_SKIP_BINARY_DOWNLOAD) {
  process.exit(0)
}

const platformPath = getPlatformPath()

if (isInstalled()) {
  process.exit(0)
}

const arch = process.arch === 'x32' ? 'ia32' : process.arch;

// downloads if not cached
download(version, {
  mirrorOptions: {
    mirror: 'https://zive.s3-us-west-2.amazonaws.com/electron/',
    customDir: version,
    customFileName: `${process.platform}-${arch}.zip`
  }
}).then(extractFile).catch(err => {
  console.error(err.stack)
  process.exit(1)
})

function isInstalled () {
  try {
    if (fs.readFileSync(path.join(__dirname, 'dist', 'version'), 'utf-8').replace(/^v/, '') !== version) {
      return false
    }

    if (fs.readFileSync(path.join(__dirname, 'path.txt'), 'utf-8') !== platformPath) {
      return false
    }
  } catch (ignored) {
    return false
  }
  
  const electronPath = process.env.ELECTRON_OVERRIDE_DIST_PATH || path.join(__dirname, 'dist', platformPath)
  
  return fs.existsSync(electronPath)
}

// unzips and makes path.txt point at the correct executable
function extractFile (zipPath) {
  return new Promise((resolve, reject) => {
    extract(zipPath, { dir: path.join(__dirname, 'dist') }, err => {
      if (err) return reject(err)

      fs.writeFile(path.join(__dirname, 'path.txt'), platformPath, err => {
        if (err) return reject(err)

        resolve()
      })
    })
  })
}

function getPlatformPath () {
  const platform = process.env.npm_config_platform || os.platform()

  switch (platform) {
    case 'mas':
    case 'darwin':
      return 'Electron.app/Contents/MacOS/Electron'
    case 'freebsd':
    case 'openbsd':
    case 'linux':
      return 'electron'
    case 'win32':
      return 'electron.exe'
    default:
      throw new Error('Electron builds are not available on platform: ' + platform)
  }
}
